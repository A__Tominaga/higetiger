package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogoutController {

	@Autowired
	HttpSession session;

	@GetMapping("/logout")
	public ModelAndView logout() {

		session.invalidate();

		return new ModelAndView("redirect:/login");
	}
}
