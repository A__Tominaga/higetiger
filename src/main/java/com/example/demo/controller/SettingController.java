package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.form.SettingForm;
import com.example.demo.service.BranchService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@Controller
@SessionAttributes
public class SettingController {

	@Autowired
	HttpSession session;

	@Autowired
	HttpServletRequest request;

	@Autowired
	UserService userService;

	@Autowired
	BranchService branchService;

	@Autowired
	DepartmentService departmentService;

	@GetMapping("/setting")
	public ModelAndView getSetting(RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView("redirect:/management");

		List<Branch> branches = branchService.findAllBranch(); // 支社名を全件取得
		List<Department> departments = departmentService.findAllDepartment(); // 部署名を全件取得

		session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		String strEditUserId = request.getParameter("userId");

		boolean isError = true;

		if (strEditUserId == null || strEditUserId == "") {
			isError = false;
		} else if (!strEditUserId.matches("^[0-9]+$")) {
			isError = false;
		}

		if (isError == false) {
			String errorMessage = "不正なパラメータが入力されました";
			redirectAttributes.addFlashAttribute("errorMessages", errorMessage);
			return mav; // ユーザー管理画面へリダイレクト
		}

		int editUserId = Integer.parseInt(strEditUserId);
		User editUser = userService.findByUser(editUserId);

		if (editUser == null) {
			String errorMessage = "不正なパラメータが入力されました";
			redirectAttributes.addFlashAttribute("errorMessages", errorMessage);
			return mav; // ユーザー管理画面へリダイレクト
		}

		if (loginUser.getId() == editUserId) {
			List<Branch> branch = new ArrayList<Branch>();
			List<Department> department = new ArrayList<Department>();
			branch.add(branches.get(0));
			department.add(departments.get(0));
			branches = branch;
			departments = department;
		}

		mav.addObject("loginUser", loginUser);
		mav.addObject("branches", branches); // 準備した空の entity を保管
		mav.addObject("departments", departments); // 準備した空の entity を保管
		mav.addObject("editUser", editUser); // 準備した空の entity を保管
		mav.setViewName("/setting"); // 画面遷移先を指定
		return mav;
	}

	@PostMapping("/update")
	public ModelAndView postSetting(@ModelAttribute("editUser") SettingForm form) {

		List<String> errorMessages = new ArrayList<String>();


		User editUser = userService.findByUser(Integer.parseInt(form.getId()));
		editUser.setAccount(form.getAccount());
		editUser.setBranchId(form.getBranchId());
		editUser.setDepartmentId(form.getDepartmentId());
		editUser.setName(form.getName());
		session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		editUser.setPassword(loginUser.getPassword());
		if(!StringUtils.isBlank(form.getPassword())) {
			editUser.setPassword(form.getPassword());
		}

		if (!isValid(errorMessages, editUser, form)) {
			ModelAndView mav = new ModelAndView();
			List<Branch> branches = branchService.findAllBranch(); // 支社名を全件取得
			List<Department> departments = departmentService.findAllDepartment(); // 部署名を全件取得
			mav.addObject("branches", branches);
			mav.addObject("departments", departments);
			String account = form.getAccount();
			String name = form.getName();
			mav.addObject("loginUser", loginUser);
			mav.addObject("editUser", editUser);
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("account", account);
			mav.addObject("name", name);
			mav.setViewName("/setting");
			return mav;
		}


		userService.updateAccountAndPassword(editUser, form.getPassword());

		return new ModelAndView("redirect:/management");

	}

	private boolean isValid(List<String> errorMessages, User editUser, SettingForm form) {

		int id = Integer.parseInt(form.getId());
		String account = form.getAccount();
		String password = form.getPassword();
		String checkPassword = form.getCheckPassword();
		String name = form.getName();
		int branchId = form.getBranchId();
		int departmentId = form.getDepartmentId();

		User duplicationUser = userService.findByAccount(account);

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (6 > account.length()) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		} else if (!account.matches("^([a-zA-Z0-9]{6,20})$")) {
			errorMessages.add("アカウント名は半角英数字かつ6文字以上20字以下で入力してください");
		}  else if (duplicationUser != null && duplicationUser.getId() != id) {
			errorMessages.add("アカウント名が重複しています");
		}

		if (!StringUtils.isBlank(password)) {
			if (6 > password.length()) {
				errorMessages.add("パスワードは6文字以上で入力してください");
			} else if (20 < password.length()) {
				errorMessages.add("パスワードは20文字以下で入力してください");

			} else if (!password.matches
				("^[a-zA-Z0-9!\"#$%&'()=-~^|\\`@{[+;*:}]<,>.?/_]{6,20}+$")) {
				errorMessages.add("パスワードは半角英数字記号のみ、6文字以上20字以下で入力してください");
			}

			if (!password.equals(checkPassword)) {
				errorMessages.add("パスワードが一致しません");
			}
		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("ユーザー名を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}

		if ((Integer.toString(branchId)) == null && !(Integer.toString(branchId)).trim().isEmpty()) {
			errorMessages.add("支社を入力してください");
		}

		if (Integer.toString(departmentId) == null && !(Integer.toString(departmentId)).trim().isEmpty()) {
			errorMessages.add("部署名を入力してください");
		}

		if (branchId == 1) {	// 本社
			if (!(departmentId == 1 || departmentId == 2)) {	// 総務人事と情報管理以外
				errorMessages.add("支社と部署の組合せが不適切です");
			}
		} else {	// 支社
			if (!(departmentId == 3 || departmentId == 4)) {	// 営業と技術部以外
				errorMessages.add("支社と部署の組合せが不適切です");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}

}
