package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@Controller
public class ManagementController {

	@Autowired
	UserService userService;

	@Autowired
	HttpSession session;

	@GetMapping("/management")
	public ModelAndView getManagement() {
		ModelAndView mav = new ModelAndView();

		List<User> users = userService.findAllUser(); // ユーザー情報を全件取得

		mav.addObject("userInfo", users); // 準備した空の entity を保管

		User user = (User)session.getAttribute("loginUser");
		mav.addObject("loginUser", user);

		mav.setViewName("management");

		return mav;

	}
}
