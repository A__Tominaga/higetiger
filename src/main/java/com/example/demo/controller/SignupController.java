package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.form.UserForm;
import com.example.demo.service.BranchService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@Controller
public class SignupController {

	@Autowired
	UserService userService;

	@Autowired
	BranchService branchService;

	@Autowired
	DepartmentService departmentService;

	@GetMapping("/signup")
	public ModelAndView getSignup() {
		ModelAndView mav = new ModelAndView();
		List<Branch> branches = branchService.findAllBranch(); // 支社名を全件取得
		List<Department> departments = departmentService.findAllDepartment(); // 部署名を全件取得
		User user = new User();
		user.setBranchId(1);
		user.setDepartmentId(1);
		mav.setViewName("/signup");
		mav.addObject("enteredUser", user);
		mav.addObject("branchId", branches); // 準備した空の entity を保管
		mav.addObject("departmentId", departments); // 準備した空の entity を保管
		return mav;
	}

	@PostMapping("/signup")
	public ModelAndView addUser(RedirectAttributes redirectAttributes,
			@ModelAttribute("User") UserForm userForm) {
		User user = new User();
		user.setAccount(userForm.getAccount());
		user.setName(userForm.getName());
		user.setPassword(userForm.getPassword());
		user.setBranchId(userForm.getBranchId());
		user.setDepartmentId(userForm.getDepartmentId());

		List<String> errorMessages = new ArrayList<String>();

		if(!isValid(errorMessages, user, userForm.getCheckPassword())) {
			List<Branch> branches = branchService.findAllBranch(); // 支社名を全件取得
			List<Department> departments = departmentService.findAllDepartment(); // 部署名を全件取得
			ModelAndView mav = new ModelAndView();
			mav.addObject("branchId", branches); // 準備した空の entity を保管
			mav.addObject("departmentId", departments); // 準備した空の entity を保管
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("enteredUser", user);
			mav.setViewName("signup");
			return mav;
		}

		userService.saveUser(user); // ユーザーをユーザーテーブルに格納

		return new ModelAndView("redirect:/management"); // ユーザー管理画面へリダイレクト
	}

	public boolean isValid(List<String> errorMessages, User user, String enteredPassword) {
		String account = user.getAccount();
		String password = user.getPassword();
		String checkPassword = enteredPassword;
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();
		User userAlreadyUsed = userService.findByAccount(account);

		if(StringUtils.isBlank(account)) {
			errorMessages.add("アカウントが入力されていません。");
		}
		if(StringUtils.isBlank(password)) {
			errorMessages.add("パスワードが入力されていません。");
		}
		if(StringUtils.isBlank(name)) {
			errorMessages.add("ユーザ名を入力してください。");
		}
		if(!StringUtils.isBlank(account) && account.length() > 20) {
			errorMessages.add("アカウントは20文字以下で入力してください。");
		}
		if(!StringUtils.isBlank(account) && account.length() < 6 ) {
			errorMessages.add("アカウントは6文字以上で入力してください。");
		}
		if(!StringUtils.isBlank(account) && (!account.matches("^[a-zA-Z0-9]+$"))) {
			errorMessages.add("アカウントに使用できる文字は半角英数字のみです。");
		}
		if(!StringUtils.isBlank(password) && password.length() > 20) {
			errorMessages.add("パスワードは20文字以下で入力してください。");
		}
		if(!StringUtils.isBlank(password) && password.length() < 6 ) {
			errorMessages.add("パスワードは6文字以上で入力してください。");
		}
		if(!StringUtils.isBlank(password) && (!password.matches("^[\\p{Alnum}|\\p{Punct}]+$"))) {
			errorMessages.add("パスワードに使用できる文字は半角英数字と記号のみです。");
		}
		if(branchId == 1 && !(departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが不正です。");
		}
		if(branchId != 1 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが不正です。");
		}
		if(!StringUtils.isBlank(name) && name.length() > 10 ) {
			errorMessages.add("名前は10文字以下で入力してください。");
		}
		if(!StringUtils.isBlank(password) && !password.equals(checkPassword)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません。");
		}
		if(userAlreadyUsed != null) {
			errorMessages.add("アカウントが重複しています。");
		}

		if(errorMessages.size() != 0){
			return false;
		}
		return true;
	}
}