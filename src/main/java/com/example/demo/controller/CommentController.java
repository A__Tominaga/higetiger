package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Comment;
import com.example.demo.entity.User;
import com.example.demo.form.CommentForm;
import com.example.demo.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	HttpSession session;

	@Autowired
	HttpServletRequest request;

	@Autowired
	CommentService commentService;

	@GetMapping("/addComment/{messageId}")
	public ModelAndView comment() {
		ModelAndView mav = new ModelAndView();
		session = request.getSession();
		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		mav.setViewName("/");
		return mav;
	}

	@PostMapping("/addComment/{messageId}")
	public ModelAndView addComment(RedirectAttributes redirectAttributes,
			@ModelAttribute("Comment") CommentForm form) {
		Comment comment = new Comment();
		session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		comment.setText(form.getText());
		comment.setMessageId(form.getMessageId());

        if (!isValid(errorMessages, comment)) {
        	ModelAndView mav = new ModelAndView("redirect:/");
			redirectAttributes.addFlashAttribute("errorMessages", errorMessages);
			redirectAttributes.addFlashAttribute("comment", comment);
			return mav;
        }

		//仮実装部分 ログイン機能実装後に実装
		comment.setUserId(user.getId());
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

	// 返信を削除
	@PostMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

	private boolean isValid(List<String> errorMessages, Comment comment) {

        if (StringUtils.isBlank(comment.getText())) {
            errorMessages.add("コメントを入力してください");

        } else if (500 < comment.getText().length()) {
            errorMessages.add("500文字以下で入力してください");

        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
	}
}