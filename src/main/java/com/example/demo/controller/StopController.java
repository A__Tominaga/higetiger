package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@Controller
public class StopController {

	@Autowired
	UserService userService;

	@PostMapping("/management/status/{id}")
	public ModelAndView stop(@PathVariable int id, @RequestParam(name="isStopped") boolean isStopped) {
		User user = userService.selectUserById(id);
		user.setIsStopped(isStopped);
		userService.StatusChange(user);
		return new ModelAndView("redirect:/management");
	}

}