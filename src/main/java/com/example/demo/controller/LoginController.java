package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.form.UserForm;
import com.example.demo.service.UserService;

@Controller
public class LoginController {

	@Autowired
	UserService userService;

	@Autowired
	HttpSession session;

	@Autowired
	HttpServletRequest request;

	@GetMapping("/login")
	public ModelAndView showLogin() {
		ModelAndView mav = new ModelAndView();
		System.out.println(session.getAttribute("errorMessages"));
		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		mav.setViewName("/login");
		session.removeAttribute("errorMessages");
		return mav;
	}

	@PostMapping("/login")
	public ModelAndView doLogin(@ModelAttribute("userForm") UserForm userForm) {
		User user = new User();
		user.setAccount(userForm.getAccount());
		user.setPassword(userForm.getPassword());
		List<String> errorMessages = new ArrayList<String>();
		User selectedUser = userService.findByAccountAndPassword(user);

		ModelAndView mav = new ModelAndView();
		if(!isValid(user, selectedUser, errorMessages)) {
			if(userForm.getAccount() != null) {
				mav.addObject("account", userForm.getAccount());
			}
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/login");
			return mav;
		}
		session.setAttribute("loginUser", selectedUser);
		User loginUser =(User)session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);
		return new ModelAndView("redirect:/");
	}

	public boolean isValid(User user, User selectedUser, List<String> errorMessages) {


		String enteredAccount = user.getAccount();
		String enteredPassword = user.getPassword();
		if(StringUtils.isEmpty(enteredAccount)) {
			errorMessages.add("アカウント名を入力してください");
		}
		if(StringUtils.isEmpty(enteredPassword)) {
			errorMessages.add("パスワードを入力してください");
		}
		if(selectedUser == null &&
				(!StringUtils.isEmpty(enteredAccount) && !StringUtils.isEmpty(enteredPassword))) {
			errorMessages.add("アカウント名またはパスワードが間違っています");
		}
		if(selectedUser != null &&selectedUser.getIsStopped() == true) {
			errorMessages.add("アカウント名またはパスワードが間違っています");
		}
		if(errorMessages.size() != 0 ) {
			return false;
		}

		return true;
	}
}