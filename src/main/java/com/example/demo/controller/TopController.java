package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;

@Controller
public class TopController {

	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpSession session;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top() {
	ModelAndView mav = new ModelAndView();
	// 投稿を全件取得

	String start = request.getParameter("start");
	String end = request.getParameter("end");
	String category = request.getParameter("category");

	session = request.getSession();
	User loginUser = (User) session.getAttribute("loginUser");

	List<Message> messageData = messageService.findAllMessage(start, end, category);
	List<Comment> commentData = commentService.findAllComment();
	// 画面遷移先を指定
	mav.setViewName("/top");
	// 投稿データオブジェクトを保管
	mav.addObject("loginUser", loginUser);
	mav.addObject("managementErrorMessages", session.getAttribute("managementErrorMessages"));
	session.removeAttribute("managementErrorMessages");
	mav.addObject("messages", messageData);
	mav.addObject("comments", commentData);
	mav.addObject("start", start);
	mav.addObject("end", end);
	mav.addObject("category", category);

	return mav;

	}

	// 投稿を削除
	@PostMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		messageService.deleteMessage(id);
		return new ModelAndView("redirect:/");
	}
}