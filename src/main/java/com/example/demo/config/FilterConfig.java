package com.example.demo.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.filter.LoginFilter;
import com.example.demo.filter.ManagementFilter;

@Configuration
public class FilterConfig {

	@Bean
	public FilterRegistrationBean<LoginFilter> loginFilter() {

		FilterRegistrationBean<LoginFilter> bean = new FilterRegistrationBean<LoginFilter>(new LoginFilter());

		bean.addUrlPatterns("/message");
		bean.addUrlPatterns("/management");
		bean.addUrlPatterns("/setting");
		bean.addUrlPatterns("/signup");
		bean.addUrlPatterns("/");
		bean.setOrder(1);

		return bean;

	}

	@Bean
	public FilterRegistrationBean<ManagementFilter> managementFilter() {

		FilterRegistrationBean<ManagementFilter> bean = new FilterRegistrationBean<ManagementFilter>(new ManagementFilter());

		bean.addUrlPatterns("/management");
		bean.addUrlPatterns("/setting");
		bean.addUrlPatterns("/signup");

		bean.setOrder(2);

		return bean;

	}

}
