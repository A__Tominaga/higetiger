package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Branch;
import com.example.demo.repository.BranchRepository;


@Service
public class BranchService {

	@Autowired
	BranchRepository branchRepository;

	/**
	* レコード全件取得
	*
	*/
	public List<Branch> findAllBranch() {
		return branchRepository.findAll();
	}

//	/**
//	* レコード1件取得
//	*
//	*/
//	public Branch findByBranch(Integer id) {
//		Branch branches = (Branch) branchRepository.findById(id).orElse(null);
//		return branches;
//	}

}
