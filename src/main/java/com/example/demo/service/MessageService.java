package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;



@Service
@Transactional
public class MessageService {

	@Autowired(required=false)
	MessageRepository messageRepository;


	@Autowired(required=false)
	// レコード全件取得
	public List<Message> findAllMessage(String start, String end, String category) {

		if (StringUtils.isBlank(category)) {
			category = null;
			//SQLにもっていかないbcSQLではnullの場合検索できないから
		}else {
			category = '%' + category + '%';
		}

		if (StringUtils.isEmpty(start)) {
			start =  "2020-01-01 00:00:00";


		}else {
			start = start + " 00:00:00";
		}

		if (StringUtils.isEmpty(end)) {
			Date date = new Date();
			end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		}else {
			end = end + " 23:59:59";
		}

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {

			Date startDate = sdFormat.parse(start);
			Date endDate = sdFormat.parse(end);
			return messageRepository.findByMessagesCreatedDateBetween(startDate, endDate, category);

		} catch (ParseException e) {

			e.printStackTrace();
		}
		return null;

	}

	public void saveMessage(Message message) {
		messageRepository.save(message);
	}

	public void deleteMessage(Integer id) {
		messageRepository.deleteById(id);
	}

	public Message findById(Integer id) {
		Message message = (Message) messageRepository.findById(id).orElse(null);
		return message;
		}
}

//public Report editReport(Integer id) {
//Report report = (Report) reportRepository.findById(id).orElse(null);
//return report;
//}