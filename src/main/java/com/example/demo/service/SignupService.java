package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;


@Service
public class SignupService {

	@Autowired
	UserRepository userRepository;

	/**
	* レコード全件取得
	*
	*/
	public List<User> findAllUser() {
		return userRepository.findAll();
	}

	/**
	* レコード追加
	*
	*/
	public void saveUser(User users) {
		userRepository.save(users);
	}
}
