package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;



@Transactional
@Service
public class CommentService {

	@Autowired(required=false)
	CommentRepository commentRepository;

//	レコード全件取得
	public List<Comment> findAllComment() {
	return commentRepository.findAll();
	}

	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
