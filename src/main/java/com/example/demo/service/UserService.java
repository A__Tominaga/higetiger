package com.example.demo.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;


@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public User findByAccountAndPassword(User user){

		String encPassword = getSHA256(user.getPassword());

		return userRepository.findByAccountAndPassword(user.getAccount(), encPassword);
	}


	public List<User> findAllUser() {
		return userRepository.findAll();
	}

	public static String getSHA256(String input){

		String toReturn = null;
		try {
		    MessageDigest digest = MessageDigest.getInstance("SHA-256");
		    digest.reset();
		    digest.update(input.getBytes("utf8"));
		    toReturn = String.format("%064x", new BigInteger(1, digest.digest()));
		} catch (Exception e) {
		    e.printStackTrace();
		}

		return toReturn;
	}

	public void saveUser(User users) {
		String encPassword = getSHA256(users.getPassword());
		users.setPassword(encPassword);
		userRepository.save(users);
	}

	public User findByUser(Integer id) {
		User editUser = (User) userRepository.findById(id).orElse(null);
		return editUser;
	}

	public User selectUserById(int id) {
		User user = (User) userRepository.findById(id).orElse(null);
		return user;
	}

	public void updateAccount(User user) {
		userRepository.save(user);
	}

	public void StatusChange(User user) {
		userRepository.save(user);
	}

	public User findByAccount(String account) {
		return userRepository.findByAccount(account);
	}

	public void updateAccountAndPassword(User user, String formPassword) {
		if(!StringUtils.isBlank(formPassword)) {
			String password = user.getPassword();

			String encPassword = getSHA256(password);

			user.setPassword(encPassword);
		}

		userRepository.save(user);

	}
}