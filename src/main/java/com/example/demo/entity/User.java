package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "users")
public class User{

	@Id
	@Column

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="account")
	private String account;

	@Column(name="password")
	private String password;

	@Column(name="name")
	private String name;


	@Column(name="branch_id")
	private int branchId;

	@Column(name="department_id")
	private int departmentId;

	@Column(name="is_stopped")
	private boolean isStopped;

	@Basic(optional = false)
	@Column(name = "created_date", insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Basic(optional = false)
	@Column(name = "updated_date", insertable = false, updatable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@ManyToOne
	   @JoinColumn(name="branch_id", insertable = false, updatable = false)
	    private Branch branch;

	@ManyToOne
	   @JoinColumn(name="department_id", insertable = false, updatable = false)
	    private Department department;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public boolean getIsStopped() {
		return isStopped;
	}

	public void setIsStopped(boolean isStopped) {
		this.isStopped = isStopped;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Branch getBranch() {
        return branch;
    }

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Department getDepartment() {
        return department;
    }

	public void setDepartment(Department department) {
		this.department = department;
	}

}