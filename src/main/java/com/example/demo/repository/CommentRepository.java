package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	@Query(value= "SELECT * FROM comments INNER JOIN users ON comment.user_id = users.id", nativeQuery = true)
    List<Comment> find(@Param("userId") Integer userId);

}
