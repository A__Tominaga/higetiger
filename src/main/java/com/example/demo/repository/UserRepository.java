package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	User findByAccountAndPassword(String account, String encPassword);

	@Query(value= "SELECT * FROM users "
			+ "INNER JOIN branches ON users.branch_id = branches.id "
			+ "RIGHT JOIN departments ON users.department_id = departments.id",
			nativeQuery = true)
    List<User> find(@Param("branchId") Integer branchId, @Param("departmentId") Integer departmentId);

	User findByAccount(String account);
}
