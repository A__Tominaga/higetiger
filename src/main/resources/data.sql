/* 利用者マスタのデータ */
INSERT INTO users (id, account, password, name, branch_id, department_id, is_stopped)
VALUES(1, 'test01', 'password', '山田太郎', 1, 1, TRUE);

/* 投稿テーブル */
INSERT INTO messages (id, title, text, category, user_id)
VALUES(1, '投稿テスト', '山田の投稿テストです', 'テスト', 1);

/* コメントテーブル */
INSERT INTO comments (id, text, user_id, message_id)
VALUES(1, 'コメントテストです', 1, 1);

/* 支社マスタのテーブル */
INSERT INTO branches (id, name)
VALUES(1, '本社');

/* 部署マスタのテーブル */
INSERT INTO departments (id, name)
VALUES(1, '総務人事部');